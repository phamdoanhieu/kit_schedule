package com.example.KitSchedule;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.KitSchedule.api.RetrofitClient;
import com.example.KitSchedule.schedule.LessonElement;
import com.google.gson.Gson;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import javax.security.auth.callback.UnsupportedCallbackException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = "Main Activity";
    private RecyclerView mRecyclerView;
    private RecyclerAdapter mRecyclerAdapter;
    private static  ArrayList<LessonElement> mLessonElements;
    Gson gson = new Gson();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mLessonElements= new ArrayList<>();
        GetDataWithAPI();
        try {
            RecyclerViewProcedure();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }

    public void RecyclerViewProcedure() throws FileNotFoundException {
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_items);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        String read = readFromFile(MainActivity.this);
        Gson gson = new Gson();

        // 1. JSON file to Java object
        Feed mFeed = gson.fromJson(read,Feed.class);
        Log.e(TAG, "Response file: " + read);

        mLessonElements.addAll(mFeed.getSchedule().get(0).getLessons());
        //đưa dữ liệu vào Adapter
        mRecyclerAdapter = new RecyclerAdapter(MainActivity.this,mLessonElements);
        mRecyclerView.setAdapter(mRecyclerAdapter);
    }
    public void GetDataWithAPI()
    {
        Call<Feed> call = RetrofitClient.getInstance().getApi().getData("CT030419","p02t08c2019");
        call.enqueue(new Callback<Feed>() {
            @Override
            public void onResponse(Call<Feed> call, Response<Feed> response) {

                String jsonInString = gson.toJson(response.body());
                writeToFile(jsonInString,MainActivity.this);

//                mLessonElements.addAll(response.body().getSchedule().get(0).getLessons());
//                mRecyclerAdapter = new RecyclerAdapter(MainActivity.this,mLessonElements);
//                mRecyclerView.setAdapter(mRecyclerAdapter);

                Log.e(TAG, "Response code: " + response.code());
                Log.e(TAG, "Response body: " + response.body());
                Log.e(TAG, "Response json: " + jsonInString);
               // Log.e(TAG, "Response read: " + read);
            }

            @Override
            public void onFailure(Call<Feed> call, Throwable t) {

                Log.e(TAG, "onFailure: " + t);
            }
        });
    }
    private void writeToFile(String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("schedule.json", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
    private String readFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("schedule.json");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append("\n").append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

}
