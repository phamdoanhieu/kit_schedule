package com.example.KitSchedule.schedule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class Schedule {
    @SerializedName("date")
    @Expose
    private Long date;
    @SerializedName("lessons")
    @Expose
    private ArrayList<LessonElement> lessons;

    public Schedule(Long date, ArrayList<LessonElement> lessons) {
        this.date = date;
        this.lessons = lessons;
    }

    public Long getDate() {
        return date;
    }

    public ArrayList<LessonElement> getLessons() {
        return lessons;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "date=" + date +
                ", lessons=" + lessons +
                '}';
    }
}
