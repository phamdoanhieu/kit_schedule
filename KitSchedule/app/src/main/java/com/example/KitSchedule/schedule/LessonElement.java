package com.example.KitSchedule.schedule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LessonElement {

    @SerializedName("lesson")
    @Expose
    private String lesson;
    @SerializedName("subject_name")
    @Expose
    private String subject_name;
    @SerializedName("address")
    @Expose
    private String address;

    public LessonElement(String lesson, String subject_name, String address) {
        this.lesson = lesson;
        this.subject_name = subject_name;
        this.address = address;
    }

    public String getLesson() {
        return lesson;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "LessonElement{" +
                "lesson='" + lesson + '\'' +
                ", subject_name='" + subject_name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
