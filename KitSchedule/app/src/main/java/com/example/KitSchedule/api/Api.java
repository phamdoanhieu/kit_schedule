package com.example.KitSchedule.api;

import com.example.KitSchedule.Feed;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {
    @GET("kma")
    Call<Feed> getData(
            @Query("username") String username,
            @Query("password") String password
    );
}
