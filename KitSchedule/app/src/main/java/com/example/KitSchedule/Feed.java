package com.example.KitSchedule;

import com.example.KitSchedule.schedule.Schedule;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Feed {
    @SerializedName("task")
    @Expose
    private String task;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("student_id")
    @Expose
    private String student_id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("class_name")
    @Expose
    private String class_name;
    @SerializedName("major")
    @Expose
    private String major;
    @SerializedName("schedule")
    @Expose
    private List<Schedule> schedule;

    public Feed(String task, Boolean success, String student_id, String name, String class_name, String major, List<Schedule> schedule) {
        this.task = task;
        this.success = success;
        this.student_id = student_id;
        this.name = name;
        this.class_name = class_name;
        this.major = major;
        this.schedule = schedule;
    }

    public String getTask() {
        return task;
    }

    public Boolean getSuccess() {
        return success;
    }

    public String getStudent_id() {
        return student_id;
    }

    public String getName() {
        return name;
    }

    public String getClass_name() {
        return class_name;
    }

    public String getMajor() {
        return major;
    }

    public List<Schedule> getSchedule() {
        return schedule;
    }

    @Override
    public String toString() {
        return "Feed{" +
                "task='" + task + '\'' +
                ", success=" + success +
                ", student_id='" + student_id + '\'' +
                ", name='" + name + '\'' +
                ", class_name='" + class_name + '\'' +
                ", major='" + major + '\'' +
                ", schedule=" + schedule +
                '}';
    }
}
