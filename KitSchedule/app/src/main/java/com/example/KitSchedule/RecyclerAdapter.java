package com.example.KitSchedule;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.KitSchedule.schedule.LessonElement;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<LessonElement> lessonElements;

    public RecyclerAdapter(Context mContext, ArrayList<LessonElement> lessonElements) {
        this.mContext = mContext;
        this.lessonElements = lessonElements;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View tkbView = inflater.inflate(R.layout.item_tkb, parent, false);
        ViewHolder viewHolder = new ViewHolder(tkbView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        LessonElement lessonElement = lessonElements.get(position);
        String lesson = lessonElements.get(position).getLesson();
        String subjectName = lessonElements.get(position).getSubject_name();
        String address = lessonElements.get(position).getAddress();
        holder.tv_lesson.setText("Tiết học: "+lesson);
        holder.tv_subjectName.setText("Môn học: "+subjectName);
        holder.tv_address.setText("Địa điểm: "+address);


    }

    @Override
    public int getItemCount() {
        return lessonElements == null ? 0 : lessonElements.size();

    }

    public class ViewHolder extends  RecyclerView.ViewHolder{
        private TextView tv_lesson;
        private TextView tv_subjectName;
        private TextView tv_address ;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_lesson = (TextView) itemView.findViewById(R.id.tv_lesson);
            tv_subjectName = (TextView) itemView.findViewById(R.id.tv_subjectName);
            tv_address = (TextView) itemView.findViewById(R.id.tv_address);

        }
    }
}
